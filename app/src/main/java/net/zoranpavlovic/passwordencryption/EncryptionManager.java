package net.zoranpavlovic.passwordencryption;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.preference.PreferenceManager;
import android.security.KeyPairGeneratorSpec;
import android.security.keystore.KeyGenParameterSpec;
import android.security.keystore.KeyProperties;
import android.support.annotation.RequiresApi;
import android.util.Base64;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyPairGenerator;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SecureRandom;
import java.security.UnrecoverableEntryException;
import java.security.cert.CertificateException;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.util.ArrayList;
import java.util.Calendar;
import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.CipherOutputStream;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.security.auth.x500.X500Principal;

/**
 * Created by osx on 16/08/2017.
 */

public class EncryptionManager {

    private static final String ENCRYPTED_KEY = "encrypted_key";
    private static final String ENCRYPTED_PASSWORD = "encrypted_password";
    private static final String RSA_KEY_ALIAS = "RSA_KEY_ALIAS";
    private static final String AndroidKeyStore = "AndroidKeyStore";
    private static final String AES_MODE = "AES/CBC/PKCS7Padding";
    private static final String KEY_ALIAS = "SWAYPAY_ALIAS_KEY";
    private static final String RSA_CIPHER = KeyProperties.KEY_ALGORITHM_RSA + "/" + KeyProperties.BLOCK_MODE_ECB + "/" + KeyProperties.ENCRYPTION_PADDING_RSA_PKCS1;
    private static final String SSL_PROVIDER = "AndroidOpenSSL";

    private SecretKey secretKey;
    private KeyStore keyStore;
    private IvParameterSpec ivParameterSpec;
    private Cipher cipher;
    private RSAPublicKey publicKey;
    private RSAPrivateKey privateKey;

    public EncryptionManager(Context context) {
        setUpEncryption(context);
    }

    private void setUpEncryption(Context context) {
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                secretKey = generateKeyForApi23AndAbove();
            } else {
                generateSecretKeyBelowApi23(context);
            }
            cipher = Cipher.getInstance(AES_MODE);
            byte[] iv = new byte[cipher.getBlockSize()];
            ivParameterSpec = new IvParameterSpec(iv);
        }  catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        }
    }

    private void generateSecretKeyBelowApi23(Context context) throws NoSuchAlgorithmException, NoSuchPaddingException {
        try {
            generateRSAKeys(context);
            loadRSAKeys();
            String enryptedKeyB64 = getEncrypretRsaKeyFromMemory(context);
            if(enryptedKeyB64 == null) {
                secretKey = generateAESKey(context);
            } else{
                secretKey = getSecretKeyForApiBelow23(context);
            }
        } catch (KeyStoreException e) {
            e.printStackTrace();
        } catch (UnrecoverableEntryException e) {
            e.printStackTrace();
        } catch (CertificateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NoSuchProviderException e) {
            e.printStackTrace();
        } catch (InvalidAlgorithmParameterException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        }
    }

    void generateRSAKeys(Context context) throws NoSuchProviderException, NoSuchAlgorithmException, InvalidAlgorithmParameterException, KeyStoreException, IOException, CertificateException {

        keyStore = KeyStore.getInstance(AndroidKeyStore);
        keyStore.load(null);

        if (!keyStore.containsAlias(RSA_KEY_ALIAS)) {
            Calendar start = Calendar.getInstance();
            Calendar end = Calendar.getInstance();
            end.add(Calendar.YEAR, 25);

            KeyPairGenerator keyGen = KeyPairGenerator.getInstance(KeyProperties.KEY_ALGORITHM_RSA, AndroidKeyStore);

            KeyPairGeneratorSpec spec = new KeyPairGeneratorSpec.Builder(context)
                    .setAlias(RSA_KEY_ALIAS)
                    .setEndDate(end.getTime())
                    .setStartDate(start.getTime())
                    .setSerialNumber(BigInteger.ONE)
                    .setSubject(new X500Principal("CN = Secured Preference Store, O = Devliving Online"))
                    .build();

            keyGen.initialize(spec);
            keyGen.generateKeyPair();
        }
    }

    void loadRSAKeys() throws KeyStoreException, UnrecoverableEntryException, NoSuchAlgorithmException {
        if (keyStore.containsAlias(RSA_KEY_ALIAS) && keyStore.entryInstanceOf(RSA_KEY_ALIAS, KeyStore.PrivateKeyEntry.class)) {
            KeyStore.PrivateKeyEntry entry = (KeyStore.PrivateKeyEntry) keyStore.getEntry(RSA_KEY_ALIAS, null);
            publicKey = (RSAPublicKey) entry.getCertificate().getPublicKey();
            privateKey = (RSAPrivateKey) entry.getPrivateKey();
        }
    }

    SecretKey generateAESKey(Context context) throws NoSuchAlgorithmException {
        final int outputKeyLength = 128;
        int saltLength = 16;
        SecureRandom random = new SecureRandom();
        byte[] salt = new byte[saltLength];
        random.nextBytes(salt);
        KeyGenerator keyGenerator = KeyGenerator.getInstance("AES");
        keyGenerator.init(outputKeyLength, random);
        SecretKey sKey = keyGenerator.generateKey();
        return generateAndStoreKeyForApiBelow23(context, sKey);
    }

    byte[] rsaEncrypt(byte[] bytes) throws KeyStoreException, UnrecoverableEntryException, NoSuchAlgorithmException, NoSuchProviderException, NoSuchPaddingException, InvalidKeyException, IOException, InvalidKeyException {
        Cipher cipher = Cipher.getInstance(RSA_CIPHER, SSL_PROVIDER);
        cipher.init(Cipher.ENCRYPT_MODE, publicKey);
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        CipherOutputStream cipherOutputStream = new CipherOutputStream(outputStream, cipher);
        cipherOutputStream.write(bytes);
        cipherOutputStream.close();

        return outputStream.toByteArray();
    }

    byte[] rsaDecrypt(byte[] bytes) throws NoSuchPaddingException, NoSuchAlgorithmException, NoSuchProviderException, InvalidKeyException, IOException {
        Cipher cipher = Cipher.getInstance(RSA_CIPHER, SSL_PROVIDER);
        cipher.init(Cipher.DECRYPT_MODE, privateKey);

        CipherInputStream cipherInputStream = new CipherInputStream(new ByteArrayInputStream(bytes), cipher);

        ArrayList<Byte> values = new ArrayList<>();
        int nextByte;
        while ((nextByte = cipherInputStream.read()) != -1) {
            values.add((byte) nextByte);
        }

        byte[] dbytes = new byte[values.size()];
        for (int i = 0; i < dbytes.length; i++) {
            dbytes[i] = values.get(i).byteValue();
        }

        cipherInputStream.close();
        return dbytes;
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private SecretKey generateKeyForApi23AndAbove() {
        SecretKey secretKey = null;
        try {
            keyStore = KeyStore.getInstance(AndroidKeyStore);
            keyStore.load(null);

            if(!keyStore.containsAlias(KEY_ALIAS)) {

                KeyGenerator keyGenerator = KeyGenerator.getInstance(KeyProperties.KEY_ALGORITHM_AES, AndroidKeyStore);
                keyGenerator.init(
                        new KeyGenParameterSpec.Builder(KEY_ALIAS,
                                KeyProperties.PURPOSE_ENCRYPT | KeyProperties.PURPOSE_DECRYPT)
                                .setBlockModes(KeyProperties.BLOCK_MODE_CBC).setEncryptionPaddings(KeyProperties.ENCRYPTION_PADDING_PKCS7)
                                .setRandomizedEncryptionRequired(false)
                                .build());
                secretKey = keyGenerator.generateKey();
            } else{
                final KeyStore.SecretKeyEntry secretKeyEntry = (KeyStore.SecretKeyEntry) keyStore
                        .getEntry(KEY_ALIAS, null);

                secretKey = secretKeyEntry.getSecretKey();
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return secretKey;
    }

    public String encrypt(String input) {
        String encryptedBase64Encoded = null;

        try {
            cipher.init(Cipher.ENCRYPT_MODE, secretKey, ivParameterSpec);
            byte[] encodedBytes = cipher.doFinal(input.getBytes());
            encryptedBase64Encoded = Base64.encodeToString(encodedBytes, Base64.DEFAULT);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return encryptedBase64Encoded;
    }

    public String decrypt(String input) {
        String decryptedText = null;
        try {
            cipher.init(Cipher.DECRYPT_MODE, secretKey, ivParameterSpec);
            byte[] bytes = Base64.decode(input, 0);
            byte[] decrypted = cipher.doFinal(bytes);
            decryptedText = new String(decrypted, "UTF-8");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return decryptedText;
    }

    private SecretKey generateAndStoreKeyForApiBelow23(Context context, SecretKey sKey){
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);
        String enryptedKeyB64 = getEncrypretRsaKeyFromMemory(context);
        if (enryptedKeyB64 == null) {
            try {
                byte[] encryptedKey = rsaEncrypt(sKey.getEncoded());
                enryptedKeyB64 = Base64.encodeToString(encryptedKey, Base64.DEFAULT);
                SharedPreferences.Editor edit = pref.edit();
                edit.putString(ENCRYPTED_KEY, enryptedKeyB64);
                edit.commit();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return sKey;
    }

    private String getEncrypretRsaKeyFromMemory(Context context){
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);
        return pref.getString(ENCRYPTED_KEY, null);
    }

    private SecretKey getSecretKeyForApiBelow23(Context context) throws NoSuchProviderException, InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, IOException {
        String enryptedKeyB64 = getEncrypretRsaKeyFromMemory(context);
        byte[] encryptedKey = Base64.decode(enryptedKeyB64, Base64.DEFAULT);
        byte[] key = rsaDecrypt(encryptedKey);
        SecretKey sk = new SecretKeySpec(key, AES_MODE);
        return sk;
    }

    public void clear(Context context) {

        try {
            keyStore = KeyStore.getInstance(AndroidKeyStore);
            keyStore.load(null);

            if (keyStore.containsAlias(KEY_ALIAS)) {
                keyStore.deleteEntry(KEY_ALIAS);
            }
            if (keyStore.containsAlias(RSA_KEY_ALIAS)) {
                keyStore.deleteEntry(RSA_KEY_ALIAS);
            }

            SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);

            //Delete key for RSA
            String enryptedKeyB64 = pref.getString(ENCRYPTED_KEY, null);
            if (enryptedKeyB64 != null) {
                pref.edit().remove(ENCRYPTED_KEY).apply();
            }

            String encryptedPassword = pref.getString(ENCRYPTED_PASSWORD, null);
            if (encryptedPassword != null) {
                pref.edit().remove(ENCRYPTED_PASSWORD).apply();
            }

        } catch (KeyStoreException e) {
            e.printStackTrace();
        } catch (CertificateException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
