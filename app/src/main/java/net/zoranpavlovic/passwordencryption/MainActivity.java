package net.zoranpavlovic.passwordencryption;

import android.app.FragmentManager;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    private static final String ENCRYPTED_PASSWORD = "encrypted_password";
    private Button btnUsePassword;
    private SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);

        btnUsePassword = (Button) findViewById(R.id.btn_show_password_dialog);

        btnUsePassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                android.support.v4.app.FragmentManager fm = getSupportFragmentManager();
                FingerprintPasswordDialog dialogFragment = new FingerprintPasswordDialog ();
                dialogFragment.show(fm, "fragment");
            }
        });
    }
}
