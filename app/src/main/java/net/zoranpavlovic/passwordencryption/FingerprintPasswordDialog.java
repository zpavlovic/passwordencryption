package net.zoranpavlovic.passwordencryption;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by osx on 16/08/2017.
 */

public class FingerprintPasswordDialog extends DialogFragment {

    private static final String ENCRYPTED_PASSWORD = "encrypted_password";
    private static final String RSA_KEY = "encrypted_key";
    private TextView fingerprintStatus;
    private EditText password;
    private Button cancelButton;
    private Button okButton;
    private Button forgotButton;
    private SharedPreferences sharedPreferences;

    public FingerprintPasswordDialog() {
        super();
    }

    public static FingerprintPasswordDialog newInstance() {
        FingerprintPasswordDialog fingerPrintDialog = new FingerprintPasswordDialog();
        Bundle args = new Bundle();
        fingerPrintDialog.setArguments(args);
        return fingerPrintDialog;
    }

    @SuppressWarnings("unchecked")
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        setStyle(android.app.DialogFragment.STYLE_NORMAL, android.R.style.Theme_Material_Light_Dialog);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = initUi(inflater);

        checkIfUserHasPassword();

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(getEncryptedPasswordFromMemory() != null){
                    boolean isCorrect = checkPassword(getEnteredPassword());
                    if(isCorrect){
                        Toast.makeText(getActivity(), "Correct password", Toast.LENGTH_SHORT).show();
                        dismiss();
                    } else{
                        Toast.makeText(getActivity(), "Wrong password", Toast.LENGTH_SHORT).show();
                    }
                } else{
                    createPassword();
                }
            }
        });

        forgotButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EncryptionManager encryptionManager = new EncryptionManager(getActivity());
                encryptionManager.clear(getActivity());
                dismiss();
            }
        });

        return view;
    }

    private void createPassword() {
        String enteredPassword = getEnteredPassword();
        String encryptedPassword = encryptPassword(enteredPassword);
        saveEncryptedPasswordIntoMemory(encryptedPassword);
        android.support.v4.app.FragmentManager fm = getActivity().getSupportFragmentManager();
        FingerprintPasswordDialog dialogFragment = new FingerprintPasswordDialog ();
        dialogFragment.show(fm, "fragment");
        dismiss();
    }

    private String encryptPassword(String enteredPassword) {
        EncryptionManager encryptionManager = new EncryptionManager(getActivity());
        return encryptionManager.encrypt(enteredPassword);
    }

    private void saveEncryptedPasswordIntoMemory(String encryptedPassword) {
        sharedPreferences.edit().putString(ENCRYPTED_PASSWORD, encryptedPassword).apply();
    }

    private String getEnteredPassword() {
        return password.getText().toString();
    }

    private void checkIfUserHasPassword() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if(getEncryptedPasswordFromMemory() != null){
                setTextForPasswordVerification();

            } else{
                setTextForCreatingPassword();
            }
        } else{
            if(getRsaKeyFromMemmory() != null && getEncryptedPasswordFromMemory() != null){
                setTextForPasswordVerification();
            } else{
                setTextForCreatingPassword();
            }
        }

    }

    private void setTextForPasswordVerification() {
        okButton.setText("Use password");
        fingerprintStatus.setText("Enter password to purchase");
    }

    private void setTextForCreatingPassword() {
        okButton.setText("Create password");
        fingerprintStatus.setText("Create your password");
    }

    @NonNull
    private View initUi(LayoutInflater inflater) {

        getDialog().setTitle("Pay with SwayPay");
        View view = inflater.inflate(R.layout.fingerprint_password_layout, null);
        getDialog().setCanceledOnTouchOutside(false);

        password = (EditText) view.findViewById(R.id.et_password);
        cancelButton = (Button) view.findViewById(R.id.cancel_button);
        okButton = (Button) view.findViewById(R.id.use_password_button);
        forgotButton = (Button) view.findViewById(R.id.forgot_button);
        fingerprintStatus = (TextView) view.findViewById(R.id.tv_fingerprint_status);

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
    }


    private boolean checkPassword(String password) {
        boolean areSame = false;
        String encryptedPasswordFromMemory = getEncryptedPasswordFromMemory();
        EncryptionManager encryptionManager = new EncryptionManager(getActivity());
        String decryptedPassword = encryptionManager.decrypt(encryptedPasswordFromMemory);
        if(password.equals(decryptedPassword)){
            areSame = true;
        }
        return areSame;
    }

    private String getEncryptedPasswordFromMemory(){
        String pw = sharedPreferences.getString(ENCRYPTED_PASSWORD, null);
        return pw;
    }

    private String getRsaKeyFromMemmory(){
        String key = sharedPreferences.getString(RSA_KEY, null);
        return key;
    }
}
